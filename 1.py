
#Python Installion & requirements
#HIDAYATULLAH ARGHANDABI     Rights Reserved 
#GitLab Repo

# Lecture 16: strings

'''



s="Hello World"
print(s)


# quotes
quote='"this quote"'
print(quote)

#print the length of the string
print(len(quote))

print(s[0]+s[1])

#start leave the first
print(s[1:])

#start from the first three
print(s[:3])

#to grab everything
print(s[:])

#start from the first last
print(s[-1])

#start leave the last letter
print(s[:-1])

#Write in reverse order
print(s[::-1])

#Write in normal order
print(s[::1])

#Write in stepping 2 order
print(s[::2])

#string has a property called immutability
#it can be changed

#s stand for hello world change h to x
# s[0]='x'  #you see an error

#cancatenate to add two strings
print(s+'Concatenate me')

#or may be
s=s+'Concatenate me all!'
print(s)

#strings or char in variable
letter='z'
print(letter)

letter*=10

print(letter)


#UpperCase
s='hello'
print(s.upper())

#LowerCase
print(s.lower())

#splittings
print(s.split('e'))
'''



#lecture 17 Print Formting

print('This is a string')

x=13.3956
print('the variable is: %s' %(x))

print('the float value is: %1.2f'%(x))


#10 points floating digits to show
print('the float value is: %1.10f'%(x))

#filling the digits to show
print('the float value is: %20.10f'%(x))

#converting to string (int else)
print('Converting to string %s'%(123))


#converting to string percent r
print('Converting to string %r' %(123))

#converting in order
print('first: %s, second: %s, third: %s' %('hi','two',25))


#Printing order
print('First: {x} Second: {y} Third: {x}'.format(x='inserted',y='two!'))
